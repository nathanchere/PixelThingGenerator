# Pixel Thing Generator
### It generates pixel... things

I wanted a way of rapidly generating artwork for a Solar Winds style game I was working on for Nintendo DS. Procedurally generated retro/pixel/whatever-you-want-to-call-it graphics are about as rapid as it gets.

Included is a basic WinForms project to demonstrate how it works:

![Pixel Thing Generator screenshot](http://nathanchere.github.io/misc/pixelthing01.gif)

[![Send me a tweet](http://nathanchere.github.io/twitter_tweet.png)](https://twitter.com/intent/tweet?screen_name=nathanchere "Send me a tweet") [![Follow me](http://nathanchere.github.io/twitter_follow.png)](https://twitter.com/intent/user?screen_name=nathanchere "Follow me")

## Status

Branch | Status | Download | Description
------|-----|------|--------
master | [![Build status](https://ci.appveyor.com/api/projects/status/olvfcuhn6qj8gxdj)](https://ci.appveyor.com/project/nathanchere/pixelthinggenerator) | [.zip](https://github.com/nathanchere/pixelthinggenerator/archive/master.zip) | Latest code

*CI generously provided by [Appveyor](http://appveyor.com)*

How I approach my public projects is explained on [my github home page](http://nathanchere.github.io).

## Release history

####v0.1 (2014-07-02)

* Procedural spaceship generation

####vAlpha (2014-07-01)

* initial release to GitHub

## Credits / thanks

* [Jared Tarbell's "Invader Fractal"](http://www.levitated.net/daily/levInvaderFractal.html) - inspiration
* [Dave Bollinger's "Pixel Spaceships"](http://www.davebollinger.com/works/pixelspaceships/) - inspiration